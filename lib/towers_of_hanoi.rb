# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.
require 'byebug'

require_relative 'user'
require_relative 'Ai_engine'

class TowersOfHanoi
  attr_reader :towers, :size
  def initialize(size = nil)
    @size = size.nil? ? 3 : size.to_i
    @towers = [[], [], []]
    @towers[0] = (1..@size).to_a.reverse
    @user = User.new(self)
  end

  def play
    render
    until won?
      from_tower, to_tower = @user.get_move
      puts(from_tower, "---", to_tower)
      next unless valid_move?(from_tower, to_tower)
      move(from_tower, to_tower)
      render
    end

    puts "CONGRATS !!!"
  end

  # private

  def auto_complete
    @user = Ai_engine.new(@towers)
    3
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    from_tower && to_tower &&
      @towers[from_tower] && @towers[to_tower] &&
      !@towers[from_tower].empty? &&
      (@towers[to_tower].empty? ||
      @towers[from_tower].last < @towers[to_tower].last)
  end

  def won?
    @towers[1..-1].any? { |tower| tower.size == @size }
  end

  def render
    system('clear')
    (@size -1).downto(0).each do |row|
      puts ""
      (0...@towers.length).each do |tower|
        print "|" + ("==" * (@towers[tower][row] || 0)).center(115 / @towers.length) + "|"
      end
    end
    puts "\n\n"
  end
end

if __FILE__ == $0
  game = TowersOfHanoi.new(ARGV[0])
  game.play
end
